const path = require('path');

const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
    entry: path.join(__dirname, 'src', 'index.jsx'),
    debug: true,
    devtool: 'source-map',
    output: {
        filename: 'app.js',
        path: path.join(__dirname, 'build'),
        publicPath: '/',
    },
    resolve: {
        extensions: ['', '.js', '.jsx', '.scss', '.css'],
        packageMains: ['browser', 'web', 'browserify', 'main', 'style'],
    },
    module: {
        loaders: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                loaders: ['babel'],
            },
            {
                test: /\.css?$/,
                loaders: ['style', 'css'],
            },
            {
                test: /\.scss$/,
                loaders: ['style', 'css?modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]', 'sass'],
            },
            {
                test: /\.(ttf|eot|svg|png|woff(2)?)$/,
                loader: 'file?name=./assets/[hash].[ext]',
            },
        ],
    },
    sassLoader: {
        data: `@import ${JSON.stringify(path.resolve(__dirname, './src/components/_toolbox_config.scss'))};`,
    },
    plugins: [
        new CopyWebpackPlugin([{ from: 'src/index.html' }]),
    ],
};
