import React from 'react';
import { Button } from 'react-toolbox/lib/button';
import { Checkbox } from 'react-toolbox/lib/checkbox';
import Input from 'react-toolbox/lib/input';
import style from './login.scss';

// let qwe;
class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value1: '',
            value2: '',
            value3: '',
            value4: '',
            value5: '',
        };

        // const value5 = false;
        this.input1Change = this.input1Change.bind(this);
        this.input2Change = this.input2Change.bind(this);
        this.btnclick = this.btnclick.bind(this);
        this.btnclick1 = this.btnclick1.bind(this);
    }

    input1Change(event) {
        this.setState({ value1: event });
    }


    input2Change(event) {
        this.setState({ value2: event });
    }


/*    btnclick() {
        const log = this.state.value1;
        const pass = this.state.value2;
//        e.preventDefault();
        if (log === '321' && pass === '456') {
            this.props.zzz();
        } else {
            this.props.ttt();
        }
    } */
    btnclick() {
        this.props.sss(this.state.value1, this.state.value2);
    }
    btnclick1(e) {
//        this.props.onClick(false);
        e.preventDefault();
        this.props.xxx();
    }

    render() {
        const omega = {
            width: '360px',
            fontSize: '20px',
            background: '#2e7d32',
            fontFamily: 'roboto',
            height: '56px',
            paddingLeft: '24px',
            paddingTop: '16',
            color: 'white',
        };

        return (
            <div className={style.flexconteiner0}>
                <div className={style.flexconteiner}>
                    <div style={omega}>Вход</div>
                    <Input className={style.flexelement1} label="Email" type="text" value={this.state.value1} onChange={this.input1Change} />
                    <Input className={style.flexelement2} label="Пароль" type="text" value={this.state.value2} onChange={this.input2Change} />
                    <div className={style.flexconteiner2}>
                        <Checkbox checked={this.state.check1} label="" />
                        <div className={style.flexelement3}>Запомнить меня</div>
                    </div>
                    <div className={style.flexconteiner3} >
                        <Button className={style.flexelement4} onClick={this.btnclick} accent flat>check</Button>
                    </div>
                    <div className={style.flexconteiner4} >
                        <a className={style.flexelement4} href="">Забыли пароль?</a>
                        <a className={style.flexelement5} onClick={this.btnclick1} href="">зарегистрироваться</a>
                    </div>
                </div>
            </div>
    );
    }
}
// export { qwe };
export default Login;
