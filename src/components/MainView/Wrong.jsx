import React from 'react';
import { Button } from 'react-toolbox/lib/button';
import style from './MainView.scss';

class Wrong extends React.Component {
    constructor(props) {
        super(props);
        this.btnclick = this.btnclick.bind(this);
    }

    btnclick() {
        this.props.www();
    }
    render() {
        return (
            <div className={style.flexconteiner}>
                <div className={style.flexelement10} > Ошибка </div>
                <div className={style.flexelement4}>Вы ввели неверный логин или пароль</div>
                <Button className={style.flexelement4} onClick={this.btnclick} accent flat>Назад</Button>
            </div>
        );
    }
}
export default Wrong;
