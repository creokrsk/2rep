import React from 'react';
import { Button } from 'react-toolbox/lib/button';
import style from './MainView.scss';

// let qwe;
class Correct extends React.Component {
    constructor(props) {
        super(props);
        this.btnclick = this.btnclick.bind(this);
    }

    btnclick() {
        this.props.vvv();
    }
    render() {
        return (
            <div className={style.flexconteiner}>
                <div className={style.flexelement10} > good! </div>
                <div className={style.flexelement4}>Авторизация прошла успешно</div>
                <Button className={style.flexelement4} onClick={this.btnclick} accent flat>OK</Button>
            </div>
        );
    }
}
export default Correct;
