import React from 'react';
import Dropdown from 'react-toolbox/lib/dropdown';
import Input from 'react-toolbox/lib/input';
import { Button } from 'react-toolbox/lib/button';
// import logo from './logo.svg';
import style from './app2.scss';


class Sel extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value1: '',
            value2: '',
            value3: '',
            value4: '',
            value5: '',
            value6: '',
            value7: '',
            value8: '',
            value9: '',
        };
        this.handleChange1 = this.handleChange1.bind(this);
        this.handleChange2 = this.handleChange2.bind(this);
        this.handleChange3 = this.handleChange3.bind(this);
        this.handleChange4 = this.handleChange4.bind(this);
        this.handleChange5 = this.handleChange5.bind(this);
        this.input1Change = this.input1Change.bind(this);
        this.input2Change = this.input2Change.bind(this);
        this.input3Change = this.input3Change.bind(this);
        this.input4Change = this.input4Change.bind(this);
        this.btnclick = this.btnclick.bind(this);
        this.btnclick2 = this.btnclick2.bind(this);
    }

    handleChange1(Vvalue) {
        this.setState({ value1: Vvalue });
    }
    handleChange2(Vvalue) {
        this.setState({ value2: Vvalue });
    }
    handleChange3(Vvalue) {
        this.setState({ value3: Vvalue });
    }
    handleChange4(Vvalue) {
        this.setState({ value4: Vvalue });
    }
    handleChange5(Vvalue) {
        this.setState({ value5: Vvalue });
    }
    input1Change(q) {
        this.setState({ value6: q });
    }
    input2Change(q) {
        this.setState({ value7: q });
    }
    input3Change(q) {
        this.setState({ value8: q });
    }
    input4Change(q) {
        this.setState({ value9: q });
    }
    btnclick2(e) {
        e.preventDefault();
        this.props.yyy();
    }
    btnclick() {
        // e.preventDefault();
        this.props.yyy(this.state.value6, this.state.value7);
    }

    render() {
        const typeorg = [
        { value: 1, label: 'usa' },
        { value: 2, label: 'russia' },
        ];
        const sfera = [
        { value: 1, label: 'government' },
        { value: 2, label: 'millitary' },
        { value: 3, label: 'police' },
        ];
        const lvl = [
        { value: 1, label: '1' },
        { value: 2, label: '2' },
        { value: 3, label: '3' },
        { value: 4, label: '80' },
        ];
        const reg = [
        { value: 1, label: 'kr' },
        { value: 2, label: 'msk' },
        { value: 3, label: 'spb' },
        { value: 4, label: 'irk' },
        ];
        const mun = [
        { value: 1, label: 'префектура' },
        { value: 2, label: 'муниципалитет' },
        { value: 3, label: 'капитолий' },
        { value: 4, label: 'грааль' },
        ];

/*    const squarestyle = {
        backgroundColor: props.pro,
        width: '300px',
        height: '300px',
        display: 'flex',
    };
*/

        return (
            <div className={style.flexconteiner10}>
                <div className={style.flexelement10}>Регистрация новой организации</div>
                <Dropdown className={style.flexelement11} source={typeorg} onChange={this.handleChange1} value={this.state.value1} label="Тип организации" />
                <Dropdown className={style.flexelement11} source={sfera} onChange={this.handleChange2} value={this.state.value2} label="Сфера деятельности" />
                <Dropdown className={style.flexelement11} source={lvl} onChange={this.handleChange3} value={this.state.value3} label="Уровень организации" />
                <Dropdown className={style.flexelement11} source={reg} onChange={this.handleChange4} value={this.state.value4} label="Регион" />
                <Dropdown className={style.flexelement11} source={mun} onChange={this.handleChange5} value={this.state.value5} label="Муниципалитет" />
                <Input className={style.flexelement12} type="text" onChange={this.input1Change} value={this.state.value6} label="Введите логин" name="name" maxLength={16} />
                <Input className={style.flexelement12} type="text" onChange={this.input2Change} value={this.state.value7} label="Введите пароль" name="name" maxLength={16} />
                <Input className={style.flexelement12} type="text" onChange={this.input3Change} value={this.state.value8} label="Подтвердите пароль" name="name" maxLength={16} />
                <Input className={style.flexelement12} type="text" onChange={this.input4Change} value={this.state.value9} label="КПП" name="name" maxLength={16} />
                <div className={style.flexconteiner11}>
                    <a className={style.flexelement13} onClick={this.btnclick2} href="">ОТМЕНИТЬ</a>
                    <Button className={style.flexelement14} onClick={this.btnclick} accent flat >Зарегистрировать</Button>
                </div>
            </div>
        );
    }
}
Sel.propTypes = {
    pro: React.PropTypes.string,
};
export default Sel;
