import React from 'react';
import { render } from 'react-dom';

// Global styles
import 'material-design-icons/iconfont/material-icons.css';
import 'react-toolbox/lib/commons.scss';
import 'roboto-fontface/css/roboto/roboto-fontface.css';

import MainView from './components/MainView/MainView';

render(<MainView />, document.getElementById('root'));
